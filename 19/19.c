#include "stdio.h"
#include "stdlib.h"

int main(int argc, char *argv[]) {
    char **diagram = NULL;

    // Get diagram
    char *line = NULL;
    long unsigned int width, height = 0, length = 0;
    while ((length = getline(&line, &length, stdin)) != -1) {
        width = length - 1;
        line[length - 1] = '\0';
        diagram = realloc(diagram, (++height) * sizeof(char *));
        diagram[height - 1] = line;
        line = NULL;
        length = 0;
    }
    printf("%lux%lu\n", height, width);
    for (int y = 0; y < height; y++) {
        printf("|%s|\n", diagram[y]);
    }

    // Find start
    unsigned int x = 0, y = 0;
    char dir;
    for (x = 0; x < width; x++) {
        if (diagram[y][x] == '|') {
            dir = 'd';
            break;
        } else if (diagram[y][x] == '-') {
            dir = 'r';
            break;
        }
    }

    // Do diagram
    printf("start = (%u, %u)\n", x, y);
    unsigned int lettercount = 0, steps = 0;
    char *letters = NULL;
    char done = 0;
    while (!done) {
        switch (dir) {
            case 'd': y += 1; break;
            case 'l': x -= 1; break;
            case 'r': x += 1; break;
            case 'u': y -= 1; break;
        }
        steps++;
        // printf("(%u, %u)\n", x, y);
        char c = diagram[y][x];
        if (c >= 'A' && c <= 'Z') {
            // printf("%c\n", c);
            letters = realloc(letters, ++lettercount);
            letters[lettercount - 1] = c;
        } else if (c == '+') {
            if (dir == 'd' || dir == 'u') {
                if (diagram[y][x - 1] != ' ') {
                    dir = 'l';
                } else if (diagram[y][x + 1] != ' ') {
                    dir = 'r';
                }
            } else if (dir == 'l' || dir == 'r') {
                if (diagram[y + 1][x] != ' ') {
                    dir = 'd';
                } else if (diagram[y - 1][x] != ' ') {
                    dir = 'u';
                }
            }
        } else if (c == ' ') {
            done = 1;
        }
    }
    letters = realloc(letters, ++lettercount);
    letters[lettercount - 1] = '\0';
    printf("letters = %s\n", letters);
    printf("steps = %u\n", steps);
    free(letters);
}
