import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;

public class Seven {
    private static class Program {
        public String name;
        public int weight;
        public Set<String> carryingNames;
        public Set<Program> carrying;
        public Program carrier;
        public int totalWeight;
        public boolean balanced;
        public Program(String name, int weight) {
            this.name = name;
            this.weight = weight;
            carryingNames = new HashSet<String>();
            carrying = new HashSet<Program>();
            carrier = null;
            totalWeight = 0;
        }
        public void computeTotalWeight() {
            int balanceWeight = -1;
            totalWeight = weight;
            balanced = true;
            for (Program program : carrying) {
                program.computeTotalWeight();
                totalWeight += program.totalWeight;
                if (balanceWeight == -1) {
                    balanceWeight = program.totalWeight;
                } else if (balanceWeight != program.totalWeight) {
                    balanced = false;
                }
            }
        }
    }

    private static Pattern top = Pattern.compile("^(\\w+) \\((\\d+)\\)$");
    private static Pattern mid = Pattern.compile("^(\\w+) \\((\\d+)\\) -> (.+)$");
    private static Pattern car = Pattern.compile("(\\w+)");

    public static void main(String[] args) {
        Map<String, Program> programs = new HashMap<String, Program>();

        Scanner sc = new Scanner(System.in);
        String line;
        while (sc.hasNextLine()) {
            line = sc.nextLine();
            Matcher topMatcher = top.matcher(line);
            if (topMatcher.matches()) {
                programs.put(topMatcher.group(1),
                    new Program(topMatcher.group(1),
                        Integer.parseInt(topMatcher.group(2))));
            }
            Matcher midMatcher = mid.matcher(line);
            if (midMatcher.matches()) {
                Program newProgram = new Program(midMatcher.group(1),
                    Integer.parseInt(midMatcher.group(2)));
                programs.put(newProgram.name, newProgram);
                Matcher carMatcher = car.matcher(midMatcher.group(3));
                while (carMatcher.find()) {
                    newProgram.carryingNames.add(carMatcher.group());
                }
            }
        }

        for (Program program : programs.values()) {
            for (String name : program.carryingNames) {
                Program carrying = programs.get(name);
                program.carrying.add(carrying);
                carrying.carrier = program;
            }
        }

        Program bottom = programs.values().iterator().next();
        while (bottom.carrier != null) {
            bottom = bottom.carrier;
        }
        System.out.println("bottom = " + bottom.name);

        bottom.computeTotalWeight();
        Program unbalanced = bottom;
        boolean stop = false;
        while (!stop) {
            stop = true;
            for (Program program : unbalanced.carrying) {
                if (!program.balanced) {
                    unbalanced = program;
                    stop = false;
                    break;
                }
            }
        }
        Map<Integer, Set<Program>> programsByWeight = new HashMap<Integer, Set<Program>>();
        for (Program program : unbalanced.carrying) {
            System.out.println(" totalWeight = " + program.totalWeight);
            for (Program subprogram : program.carrying) {
                System.out.println("   totalWeight = " + subprogram.totalWeight);
            }
            if (!programsByWeight.containsKey(program.totalWeight)) {
                programsByWeight.put(program.totalWeight,
                new HashSet<Program>());
            }
            programsByWeight.get(program.totalWeight).add(program);
        }
        Program rebalance = null;
        int targetWeight = 0;
        for (Integer totalWeight : programsByWeight.keySet()) {
            if (programsByWeight.get(totalWeight).size() == 1) {
                rebalance = programsByWeight.get(totalWeight).iterator().next();
            } else {
                targetWeight = programsByWeight.get(totalWeight).iterator().next().totalWeight;
            }
        }
        System.out.println("rebalance = " + rebalance.name);
        System.out.println("rebalance.weight = " + rebalance.weight);
        System.out.println("rebalance.totalWeight = " + rebalance.totalWeight);
        System.out.println("new weight = " + (rebalance.weight + targetWeight - rebalance.totalWeight));
    }
}
