#!/usr/bin/env python

import sys
import math

if __name__ == "__main__":
    print("3.1")
    square = int(sys.argv[1])
    print("square", square)
    side = math.ceil(math.sqrt(square))
    if side % 2 == 0:
        side += 1
    print("side", side)
    positions = 4 * (side - 1)
    print("positions", positions)
    position = square - ((side - 2) ** 2) - 1
    if position < 0:
        position = 0
    print("position", position)
    corners = [
        1 * (side - 1) - 1,
        2 * (side - 1) - 1,
        3 * (side - 1) - 1,
        4 * (side - 1) - 1]
    print("corners", corners)
    p = "unknown"
    dx = 0
    dy = 0
    if position == corners[3]:
        p = "bottom right"
        dx = -(side // 2)
        dy = +(side // 2)
    elif position < corners[0]:
        p = "right"
        dx = -(side // 2)
    elif position == corners[0]:
        p = "top right"
        dx = -(side // 2)
    elif position < corners[1]:
        p = "top"
    elif position == corners[1]:
        p = "top left"
    elif position < corners[2]:
        p = "left"
    elif position == corners[2]:
        p = "bottom left"
    elif position < corners[3]:
        p = "bottom"
        dx = corners[2] + (side // 2) - position
        dy = +(side // 2)
    else:
        p = "wat"
    print("p", p)
    print("dx", dx)
    print("dy", dy)
    print("dist", abs(dx) + abs(dy))
