#include <iostream>
#include <vector>

using namespace std;

int main (int argc, char **argv) {
    // Build array
    vector<unsigned short int> digits;
    char c;
    while (cin.get(c)) {
        if (c >= '0' && c <= '9') {
            digits.push_back(c - '0');
        }
    }

    // Loop through
    unsigned long long int length = digits.size();
    unsigned short int digit, compareTo;
    unsigned long long int sum = 0;

    // Task 1
    for (int i = 0; i < digits.size(); i++) {
        digit = digits[i];
        compareTo = digits[(i + 1) % length];
        if (digit == compareTo) {
            sum += digit;
        }
    }
    cout << sum << endl;

    // Task 2
    for (int i = 0; i < digits.size(); i++) {
        digit = digits[i];
        compareTo = digits[(i + (length / 2)) % length];
        if (digit == compareTo) {
            sum += digit;
        }
    }
    cout << sum << endl;
    return 0;
}
