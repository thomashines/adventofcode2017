import Data.List
import Data.List.Split
import System.Environment

main = do
    [inFileName] <- getArgs
    contents <- readFile inFileName
    let contentsLines = lines contents
    divs <- mapM getDiv contentsLines
    -- mapM_ (\d -> putStrLn (show d)) divs
    let checksum = sum divs
    putStrLn ("checksum = " ++ (show checksum))

getDiv :: String -> IO Integer
getDiv line = do
    -- putStrLn ("line = " ++ line)
    let values = map read (splitOn "\t" line) :: [Integer]
    -- mapM_ (\v -> putStrLn (show v)) values
    let (left, right) = getDivisbles values
    -- putStrLn ("left = " ++ (show left))
    -- putStrLn ("right = " ++ (show right))
    return (quot left right)

getDivisbles :: [Integer] -> (Integer, Integer)
getDivisbles values =
    case uncons values of
        Just (headValue, []) -> (headValue, headValue)
        Just (headValue, tailValues) ->
            case find (\tailValue -> (rem headValue tailValue) == 0) tailValues of
                Just tailValue -> (headValue, tailValue)
                Nothing -> case find (\tailValue -> (rem tailValue headValue) == 0) tailValues of
                    Just tailValue -> (tailValue, headValue)
                    Nothing -> getDivisbles tailValues
        Nothing -> (0, 1)
