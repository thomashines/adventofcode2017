#!/usr/bin/env ruby

def groupcontiguous(lines, allocations, x, y, group)
    allocations[y][x] = group
    if x > 0 && lines[y][x - 1] == "#" && allocations[y][x - 1] == -1
        groupcontiguous(lines, allocations, x - 1, y, group)
    end
    if x < 127 && lines[y][x + 1] == "#" && allocations[y][x + 1] == -1
        groupcontiguous(lines, allocations, x + 1, y, group)
    end
    if y > 0 && lines[y - 1][x] == "#" && allocations[y - 1][x] == -1
        groupcontiguous(lines, allocations, x, y - 1, group)
    end
    if y < 127 && lines[y + 1][x] == "#" && allocations[y + 1][x] == -1
        groupcontiguous(lines, allocations, x, y + 1, group)
    end
end

lines = ARGF.read.split("\n").map { |line| line.split(": ")[1] }
allocations = Array.new(128) { Array.new(128, -1) }
groups = 0
(0..127).each do |y|
    (0..127).each do |x|
        if lines[y][x] == "#" && allocations[y][x] == -1
            groupcontiguous(lines, allocations, x, y, groups)
            groups += 1
        end
    end
end

(0..127).each do |y|
    (0..127).each do |x|
        if allocations[y][x] >= 0
            print allocations[y][x] % 10
        else
            print "."
        end
    end
    print "\n"
end

puts "groups: #{groups}"
