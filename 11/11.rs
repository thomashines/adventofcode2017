use std::io::BufRead;
use std::cmp;

fn stepper((n, ne, se, max): (i32, i32, i32, i32), step: &str) -> (i32, i32, i32, i32) {
    let (nn, nne, nse) = match step.as_ref() {
        "n" => { (n + 1, ne, se) }
        "ne" => { (n, ne + 1, se) }
        "se" => { (n, ne, se + 1) }
        "s" => { (n - 1, ne, se) }
        "sw" => { (n, ne - 1, se) }
        "nw" => { (n, ne, se - 1) }
        _ => { (n, ne, se) }
    };
    return (nn, nne, nse, cmp::max(max, dist((nn, nne, nse))));
}

fn rationalise_nne((n, ne, se): (i32, i32, i32)) -> (i32, i32, i32) {
    if n != 0 && ne != 0 && n.signum() != ne.signum() {
        let dse = ne.signum() * cmp::min(n.abs(), ne.abs());
        return (n + dse, ne - dse, se + dse);
    } else {
        return (n, ne, se);
    }
}

fn dist((n, ne, se): (i32, i32, i32)) -> i32 {
    let (rn, rne, rse) = rationalise_nne((n, ne, se));
    let (rrse, rrn, rrne) = rationalise_nne((-rse, rn, rne));
    let (rrrne, rrrse, rrrn) = rationalise_nne((-rrne, rrse, rrn));
    return rrrn.abs() + rrrne.abs() + rrrse.abs();
}

fn main() {
    let stdin = std::io::stdin();
    let line = stdin.lock().lines().next().unwrap().unwrap();
    let steps = line.split(",");
    let (n, ne, se, max) = steps.fold((0, 0, 0, 0), stepper);
    println!("(n, ne, se) = ({}, {}, {})", n, ne, se);
    println!("dist = {}", dist((n, ne, se)));
    println!("max = {}", max);
}
