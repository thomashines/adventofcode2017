#!/usr/bin/env python

def primes(n):
    primfac = []
    d = 2
    while d * d <= n:
        while (n % d) == 0:
            primfac.append(d)
            n //= d
        d += 1
    if n > 1:
       primfac.append(n)
    return set(primfac)

if __name__ == "__main__":
    h = 0
    for i in range(108100, 125117, 17):
        print(i, primes(i))
        if len(primes(i)) > 1:
            h += 1
        # h += len(primes(i))
    print(h)
