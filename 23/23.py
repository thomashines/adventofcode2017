#!/usr/bin/env python

import sys


if __name__ == "__main__":
    lines = [l.rstrip().split(" ") for l in sys.stdin.readlines()]
    print(list(lines))

    registers = {}
    for l in "abcdefgh":
        registers[l] = 0
    registers["a"] = 1

    def val(v):
        if v in registers:
            return registers[v]
        return int(v)

    instructions = {
        "set": lambda x, y: val(y),
        "sub": lambda x, y: registers[x] - val(y),
        "mul": lambda x, y: registers[x] * val(y),
    }

    muls = 0
    i = 0
    while i >= 0 and i < len(lines):
        print(i + 1, registers)
        if lines[i][0] == "mul":
            muls += 1
        if lines[i][0] in instructions:
            registers[lines[i][1]] = instructions[lines[i][0]](*lines[i][1:])
            # if lines[i][1] == "h":
            #     print("h", registers["h"])
            i += 1
        elif val(lines[i][1]) != 0:
            i += val(lines[i][2])
        else:
            i += 1

    print("muls", muls)
