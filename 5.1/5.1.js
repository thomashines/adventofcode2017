fiveone = function (x) {
 var v = x.split("\n").filter(s => s !== "").map(s => Number(s));
 var steps = 0;
 for (var i = 0; i >= 0 && i < v.length;) {
  i += v[i]++;
  steps++;
 }
 return steps;
}

var client = new XMLHttpRequest();
client.open('GET', 'http://adventofcode.com/2017/day/5/input');
client.onreadystatechange = () => console.log(fiveone(client.responseText));
client.send();