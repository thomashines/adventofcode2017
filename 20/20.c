#include "stdio.h"
#include "stdlib.h"

#define X 0
#define Y 1
#define Z 2

// unsigned long long mag(long long int *v) {
//     return abs(v[X]) + abs(v[Y]) + abs(v[Z]);
// }
#define mag(v) (abs(v[X]) + abs(v[Y]) + abs(v[Z]))

// char equ(long long int *a, long long int *b) {
//     return a[X] == b[X] && a[Y] == b[Y] && a[Z] == b[Z];
// }
#define equ(a, b) (a[X] == b[X] && a[Y] == b[Y] && a[Z] == b[Z])

typedef struct Particle {
    long long int p[3];
    long long int v[3];
    long long int a[3];
    char active;
} Particle;

void particletick(Particle *particle) {
    particle->p[X] += (particle->v[X] += particle->a[X]);
    particle->p[Y] += (particle->v[Y] += particle->a[Y]);
    particle->p[Z] += (particle->v[Z] += particle->a[Z]);
}

void particleprint(Particle *particle) {
    printf("p=<%lld,%lld,%lld>[%d], v=<%lld,%lld,%lld>[%d], a=<%lld,%lld,%lld>[%d]\n",
        particle->p[X], particle->p[Y], particle->p[Z], mag(particle->p),
        particle->v[X], particle->v[Y], particle->v[Z], mag(particle->v),
        particle->a[X], particle->a[Y], particle->a[Z], mag(particle->a));
}


int main(int argc, char *argv[]) {
    unsigned int particlecount = 0;
    Particle *particles = NULL;

    unsigned long long mina = -1;
    unsigned int minaparticle = -1;

    unsigned long int length = 0;
    char *line = NULL;
    while ((length = getline(&line, &length, stdin)) != -1) {
        particles = realloc(particles, (++particlecount) * sizeof(Particle));
        particles[particlecount - 1].active = 1;
        char *param = line + 3;
        particles[particlecount - 1].p[X] = strtol(param, &param, 10);
        particles[particlecount - 1].p[Y] = strtol(++param, &param, 10);
        particles[particlecount - 1].p[Z] = strtol(++param, &param, 10);
        param += 6;
        particles[particlecount - 1].v[X] = strtol(param, &param, 10);
        particles[particlecount - 1].v[Y] = strtol(++param, &param, 10);
        particles[particlecount - 1].v[Z] = strtol(++param, &param, 10);
        param += 6;
        particles[particlecount - 1].a[X] = strtol(param, &param, 10);
        particles[particlecount - 1].a[Y] = strtol(++param, &param, 10);
        particles[particlecount - 1].a[Z] = strtol(++param, &param, 10);
        particleprint(&particles[particlecount - 1]);

        unsigned long long a = mag(particles[particlecount - 1].a);
        if (a < mina) {
            mina = a;
            minaparticle = particlecount - 1;
        }
    }
    free(line);
    printf("minaparticle = %u\n\n", minaparticle);

    unsigned int i, p0, p1, activecount = particlecount;
    for (;;) {
        for (p0 = 0; p0 < particlecount; p0++) {
            if (particles[p0].active) {
                particletick(&particles[p0]);
                // particleprint(&particles[p]);
            }
        }
        for (p0 = 0; p0 < particlecount - 1; p0++) {
            if (particles[p0].active) {
                for (p1 = p0 + 1; p1 < particlecount; p1++) {
                    if (particles[p1].active) {
                        if (equ(particles[p0].p, particles[p1].p)) {
                            if (particles[p0].active) {
                                particles[p0].active = 0;
                                printf("activecount = %u\n", --activecount);
                            }
                            if (particles[p1].active) {
                                particles[p1].active = 0;
                                printf("activecount = %u\n", --activecount);
                            }
                        }
                    }
                }
            }
        }
        // printf("\n");
    }

    return 0;
}
