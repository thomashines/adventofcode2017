using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

public class Test {

    private static Regex format = new Regex(@"^(\S+) (dec|inc) (\S+) if (\S+) (<|<=|==|!=|>=|>) (\S+)$");

    private static Dictionary<string, Func<int, int, bool>> comparisons = new Dictionary<string, Func<int, int, bool>> {
        {"<", (left, right) => left < right},
        {"<=", (left, right) => left <= right},
        {"==", (left, right) => left == right},
        {"!=", (left, right) => left != right},
        {">=", (left, right) => left >= right},
        {">", (left, right) => left > right},
    };

    private static Dictionary<string, int> registers = new Dictionary<string, int>();

    public static void Main(string[] args) {
        int maxAllTime = 0;
        string line;
        while ((line = Console.ReadLine()) != null) {
            Match deformatted = format.Match(line);
            string target = deformatted.Groups[1].Value;
            string operation = deformatted.Groups[2].Value;
            int amount = int.Parse(deformatted.Groups[3].Value);
            string left = deformatted.Groups[4].Value;
            string comparison = deformatted.Groups[5].Value;
            int right = int.Parse(deformatted.Groups[6].Value);

            // Console.WriteLine("target = " + target);
            // Console.WriteLine("operation = " + operation);
            // Console.WriteLine("amount = " + amount);
            // Console.WriteLine("left = " + left);
            // Console.WriteLine("comparison = " + comparison);
            // Console.WriteLine("right = " + right);

            if (!registers.ContainsKey(target)) {
                registers.Add(target, 0);
            }

            if (!registers.ContainsKey(left)) {
                registers.Add(left, 0);
            }

            if (operation == "dec") {
                amount *= -1;
            }

            if (comparisons[comparison](registers[left], right)) {
                registers[target] += amount;
                if (registers[target] > maxAllTime) {
                    maxAllTime = registers[target];
                }
            }
        }

        Console.WriteLine("max = " + registers.Values.Max());
        Console.WriteLine("maxAllTime = " + maxAllTime);
    }
}
