object TenOne {
    def flipSlice(rope: List[Int], size: Int, start: Int, end: Int): List[Int] = {
        // println(start, end)
        if (start < end) {
            val (left, notleft) = rope.splitAt(start)
            val (mid, right) = notleft.splitAt(end - start)
            // println(left, mid, right)
            left ++ mid.reverse ++ right
        } else {
            val (left, notleft) = rope.splitAt(end)
            val (mid, right) = notleft.splitAt(start - end)
            // println(left, mid, right)
            val reversed = (right ++ left).reverse
            reversed.drop(size - start) ++ mid ++ reversed.take(size - start)
        }
    }

    def doTurn(state: (List[Int], Int, Int, Int), length: Int): (List[Int], Int, Int, Int) = state match {
        case (rope, size, start, skip) => {
            // println(state, length)
            if (length == 0) {
                (rope, size, (start + length + skip) % size, skip + 1)
            } else {
                (flipSlice(rope, size, start, (start + length) % size), size, (start + length + skip) % size, skip + 1)
            }
        }
    }

    def main(args: Array[String]): Unit = {
        val lengths = scala.io.StdIn.readLine().split(',').toList.map(_.toInt)
        // val size = 5
        val size = 256
        val rope = (0 to (size - 1)).toList
        println(lengths)
        val (finalRope, finalSize, start, skip) = lengths.foldLeft((rope, size, 0, 0))(doTurn)
        println(finalRope, finalSize, start, skip)
        println(finalRope(0) * finalRope(1))
    }
}
