import 'dart:convert';
import 'dart:io';

void main() {
    stdin.transform(UTF8.decoder)
        .transform(new LineSplitter())
        .fold(0, solver)
        .then(print);
}

int solver(int severity, String line) {
    String split = line.split(": ");
    int depth = int.parse(split[0]);
    int range = int.parse(split[1]);
    if ((depth - range + 1) % (2 * range - 2) == range - 1) {
        return severity + depth * range;
    } else {
        return severity;
    }
}
