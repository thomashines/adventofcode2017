#include "stdio.h"
#include "stdlib.h"

typedef struct Node Node;
struct Node {
    Node *left, *right;
    unsigned int value;
};

Node *makenode(Node *left, Node *right, unsigned int value) {
    Node *node = malloc(sizeof(Node));
    node->left = left;
    node->right = right;
    node->value = value;
    return node;
}

void freenodes(Node *start) {
    Node *node = start;
    Node *freenode;
    while (node != NULL) {
        freenode = node;
        node = node->right;
        free(freenode);
        if (node == start) {
            break;
        }
    }
}

void printring(Node *start) {
    Node *node = start;
    while (node != NULL) {
        printf("%d ", node->value);
        node = node->right;
        if (node == start) {
            break;
        }
    }
    printf("\n");
}

int main (int argc, char **argv) {
    unsigned int stepsize = atoi(argv[1]);
    printf("stepsize = %d\n", stepsize);

    // This is the real shit
    unsigned int size = 1;
    unsigned int pos = 0;
    for (unsigned int value = 1; value <= 50000000; value++) {
        pos = (pos + stepsize) % size;
        if (pos == 0) {
            printf("value = %d\n", value);
        }
        size++;
        pos++;
    }

    // This is the linked list
    Node *zero = makenode(NULL, NULL, 0);
    Node *node = zero;
    node->left = node;
    node->right = node;
    unsigned int prevzero = 0;
    for (unsigned int value = 1; value <= 50000000; value++) {
        // Step forward
        for (unsigned int s = 0; s < stepsize; s++) {
            node = node->right;
        }
        // Insert
        Node *newnode = makenode(node, node->right, value);
        node->right->left = newnode;
        node->right = newnode;
        node = newnode;
        if (value == 2017) {
            printf("2017->right = %d\n", node->right->value);
        }
        if (zero->right->value != prevzero) {
            printf("zero->right = %d\n", zero->right->value);
            prevzero = zero->right->value;
        }
    }
    freenodes(node);
}
