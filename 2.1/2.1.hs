import Data.List
import Data.List.Split
import System.Environment

main = do
    [inFileName] <- getArgs
    contents <- readFile inFileName
    let contentsLines = lines contents
    diffs <- mapM getDiff contentsLines
    let checksum = sum diffs
    putStrLn ("checksum = " ++ (show checksum))

getDiff :: String -> IO Integer
getDiff line = do
    -- putStrLn ("line = " ++ line)
    let values = map read (splitOn "\t" line) :: [Integer]
    -- mapM_ (\v -> putStrLn (show v)) values
    case uncons values of
        Just (headValue, tailValues) ->
            let (minValue, maxValue) = foldl limitsFolder (headValue, headValue) tailValues
            in return (maxValue - minValue)
        Nothing -> return 0

limitsFolder :: (Integer, Integer) -> Integer -> (Integer, Integer)
limitsFolder (minValue, maxValue) value
    | value > maxValue && value < minValue = (value, value)
    | value > maxValue = (minValue, value)
    | value < minValue = (value, maxValue)
    | otherwise = (minValue, maxValue)
