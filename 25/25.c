#include "stdio.h"
#include "stdlib.h"
#include "string.h"

typedef struct State {
    char name;
    char write[2];
    int move[2];
    char nextstate[2];
} State;

typedef struct Tape {
    unsigned char *positive, *negative;
    int positivesize, negativesize;
    int minindex, maxindex;
} Tape;

void tapeexpand(Tape *tape, int index) {
    // printf("tapeexpand(%d)\n", index);
    if (index >= tape->positivesize) {
        tape->positivesize = (index + 1) * 2;
        // printf("tape->positivesize = %d\n", tape->positivesize);
        // printf("tape->maxindex = %d\n", tape->maxindex);
        tape->positive = realloc(tape->positive, tape->positivesize * sizeof(unsigned char));
        memset(&tape->positive[tape->maxindex + 1], 0, tape->positivesize - tape->maxindex - 1);
    } else if (-index >= tape->negativesize) {
        tape->negativesize = (1 - index) * 2;
        // printf("tape->negativesize = %d\n", tape->negativesize);
        // printf("tape->minindex = %d\n", tape->minindex);
        tape->negative = realloc(tape->negative, tape->negativesize * sizeof(unsigned char));
        memset(&tape->negative[1 - tape->minindex], 0, tape->negativesize + tape->minindex - 1);
    }
    if (index > tape->maxindex) {
        tape->maxindex = index;
    }
    if (index < tape->minindex) {
        tape->minindex = index;
    }
}

Tape *tapemalloc(void) {
    Tape *tape = malloc(sizeof(Tape));
    tape->positive = NULL;
    tape->negative = NULL;
    tape->positivesize = 0;
    tape->negativesize = 0;
    tape->minindex = 1;
    tape->maxindex = -1;
    return tape;
}

unsigned char tapeget(Tape *tape, int index) {
    tapeexpand(tape, index);
    if (index >= 0) {
        return tape->positive[index];
    } else {
        return tape->negative[-index];
    }
}

void tapeset(Tape *tape, int index, unsigned char value) {
    tapeexpand(tape, index);
    if (index >= 0) {
        tape->positive[index] = value;
    } else {
        tape->negative[-index] = value;
    }
}

void tapeprint(Tape *tape) {
    int index;
    for (index = tape->minindex; index <= tape->maxindex; index++) {
        printf("%d ", tapeget(tape, index));
    }
    printf("\n");
}

unsigned int tapechecksum(Tape *tape) {
    unsigned int checksum = 0;
    int index;
    for (index = tape->minindex; index <= tape->maxindex; index++) {
        checksum += tapeget(tape, index);
    }
    return checksum;
}

int main(int argc, char **argv) {
    unsigned long int length = 0;
    char *line = NULL;

    // Read initial state
    length = getline(&line, &length, stdin);
    char state = line[15];
    // printf("state = %c\n", state);

    // Read count
    length = getline(&line, &length, stdin);
    long int steps = strtol(&line[36], NULL, 10);
    // printf("steps = %ld\n", steps);

    // Read states
    unsigned int statecount = 0;
    State *states = NULL;
    while ((length = getline(&line, &length, stdin)) != -1) {
        // Make state
        states = realloc(states, (++statecount) * sizeof(State));

        // Get state name
        length = getline(&line, &length, stdin);
        states[statecount - 1].name = line[9];
        // printf("name = %c\n", states[statecount - 1].name);

        // Get the behaviours
        char i;
        for (i = 0; i < 2; i++) {
            // Get the condition
            length = getline(&line, &length, stdin);
            unsigned char value = line[26] - '0';
            // printf("value = %d\n", value);

            // Get the value to write
            length = getline(&line, &length, stdin);
            states[statecount - 1].write[value] = line[22] - '0';
            // printf("write = %d\n", states[statecount - 1].write[value]);

            // Get the direction to move
            length = getline(&line, &length, stdin);
            states[statecount - 1].move[value] = line[27] == 'r' ? 1 : -1;
            // printf("move = %d\n", states[statecount - 1].move[value]);

            // Get the state to go to next
            length = getline(&line, &length, stdin);
            states[statecount - 1].nextstate[value] = line[26];
            // printf("nextstate = %c\n", states[statecount - 1].nextstate[value]);
        }
    }

    // Do the program
    Tape *tape = tapemalloc();
    printf("state = %c, checksum = %u, tape = ", state, tapechecksum(tape));
    tapeprint(tape);
    // tapeset(tape, -1, 1);
    // tapeset(tape, 0, 0);
    // tapeset(tape, 1, 1);
    // tapeexpand(tape, -4);
    // tapeexpand(tape, 4);
    unsigned int step;
    int slot = 0;
    for (step = 0; step < steps; step++) {
        unsigned char value = tapeget(tape, slot);
        tapeset(tape, slot, states[state - 'A'].write[value]);
        slot += states[state - 'A'].move[value];
        state = states[state - 'A'].nextstate[value];
        // printf("state = %c, checksum = %u, tape = ", state, tapechecksum(tape));
        // tapeprint(tape);
    }

    printf("state = %c, checksum = %u, tape = ", state, tapechecksum(tape));
    tapeprint(tape);

    return 0;
}
