import std.stdio;
import std.string;
import std.conv;

void main() {
    int[][] graph;

    string line;
    while ((line = stripRight(stdin.readln())) !is null) {
        string[] split = line.split(" <-> ");
        int left = to!int(split[0]);
        string[] rights = split[1].split(", ");
        graph.length++;
        graph[left].length = rights.length;
        foreach (int r; 0 .. cast(int) rights.length) {
            graph[left][r] = to!int(rights[r]);
        }
    }
    int count = cast(int) graph.length;

    foreach (int n; 0 .. count) {
        foreach (int c; graph[n]) {
            graph[c].length++;
            graph[c][graph[c].length - 1] = n;
        }
    }

    // writefln("search");
    int toSearch = count;
    bool[] searched = new bool[](count);
    searched[] = false;
    int[] queue = new int[](1);
    int groups = 0;

    while (toSearch > 0) {
        // Get start
        int queued = 0;
        foreach (int n; 0 .. count) {
            if (!searched[n]) {
                writefln("start from = %d", n);
                queue[0] = n;
                queued = 1;
                break;
            }
        }

        // Get group
        int groupSize = 0;
        while (queued > 0) {
            int node = queue[--queued];
            if (!searched[node]) {
                searched[node] = true;
                groupSize++;
                toSearch--;
                foreach (int connected; graph[node]) {
                    if (!searched[connected]) {
                        if (queued + 1 >= queue.length) {
                            queue.length *= 2;
                        }
                        queue[queued++] = connected;
                    }
                }
            }
        }
        writefln("      size = %d", groupSize);
        if (groupSize > 0) {
            groups++;
        }
    }

    writefln("    groups = %d", groups);
}
