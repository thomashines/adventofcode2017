open Microsoft.FSharp.Core.Operators.Checked
open System

let streamFolder (depth, score, garbage, skip, garbageCount) char =
    match skip with
    | true -> (depth, score, garbage, false, garbageCount)
    | false ->
        match char with
        | '!' -> (depth, score, garbage, true, garbageCount)
        | _ ->
            match garbage with
            | true ->
                match char with
                | '>' -> (depth, score, false, skip, garbageCount)
                | _ -> (depth, score, garbage, skip, garbageCount + 1)
            | false ->
                match char with
                | '{' -> (depth + 1, score + depth, garbage, skip, garbageCount)
                | '}' -> (depth - 1, score, garbage, skip, garbageCount)
                | '<' -> (depth, score, true, skip, garbageCount)
                | _ -> (depth, score, garbage, skip, garbageCount)

[<EntryPoint>]
let main argv =
    let (depth, score, garbage, skip, garbageCount) =
        Seq.fold streamFolder (1, 0, false, false, 0) (Console.ReadLine ())
    printfn "score = %d" score
    printfn "garbageCount = %d" garbageCount
    0
