#!/usr/bin/env python

import sys

class Component:
    def __init__(self, a, b):
        self.a = a
        self.b = b
        if a > b:
            self.a = b
            self.b = a
        self.strength = a + b
        self.used = False
        self.maxlength = 0
        self.maxstrength = 0

    def connects(self, port):
        if self.a == port:
            return self.b
        if self.b == port:
            return self.a
        return None

    def __str__(self):
        return "Component({}/{})".format(self.a, self.b)

    def __repr__(self):
        return "<Component {} {}>".format(self.a, self.b)

class Node:
    def __init__(self, port):
        self.port = port
        self.connections = {}
        self.strengths = []
        self.visited = False

    def connect(self, component):
        self.connections[component.connects(self.port)] = component

    def __str__(self):
        return "Node({})".format(self.port)

    def __repr__(self):
        return "<Node {} [{}]>".format(self.port, len(self.connections))

def getmaxstrengths(ports, component, fromport, strength):
    if component is None:
        for toport, nextcomponent in ports[fromport].connections.items():
            getmaxstrengths(ports, nextcomponent, toport, strength)
    elif not component.used:
        component.used = True
        newstrength = strength + component.strength
        if newstrength > component.maxstrength:
            component.maxstrength = newstrength
        for toport, nextcomponent in ports[fromport].connections.items():
            getmaxstrengths(ports, nextcomponent, toport, newstrength)
        component.used = False

def getmaxlengths(ports, component, fromport, length, strength):
    if component is None:
        for toport, nextcomponent in ports[fromport].connections.items():
            getmaxlengths(ports, nextcomponent, toport, length, strength)
    elif not component.used:
        component.used = True
        newstrength = strength + component.strength
        newlength = length + 1
        if newlength > component.maxlength:
            component.maxlength = newlength
            component.maxstrength = newstrength
        for toport, nextcomponent in ports[fromport].connections.items():
            getmaxlengths(ports, nextcomponent, toport, newlength, newstrength)
        component.used = False

if __name__ == "__main__":
    components = []
    ports = {}
    for line in sys.stdin.readlines():
        a, b = [int(p) for p in line.rstrip().split("/")]
        components.append(Component(a, b))
        if a not in ports:
            ports[a] = Node(a)
        if b not in ports:
            ports[b] = Node(b)
    # print(len(components), components)
    for c in components:
        ports[c.a].connect(c)
        ports[c.b].connect(c)
    # print(len(ports), ports)

    # Calculate max strengths
    getmaxstrengths(ports, None, 0, 0)
    maxstrength = 0
    for last in components:
        if last.maxstrength > maxstrength:
            maxstrength = last.maxstrength
            print(last, maxstrength)

    # Reset strengths
    for component in components:
        component.maxstrength = 0

    # Calculate max strengths
    getmaxlengths(ports, None, 0, 0, 0)
    maxlength = 0
    maxstrength = 0
    for last in components:
        if last.maxlength > maxlength or (last.maxlength == maxlength and last.maxstrength > maxstrength):
            maxlength = last.maxlength
            maxstrength = last.maxstrength
            print(last, maxlength, maxstrength)
