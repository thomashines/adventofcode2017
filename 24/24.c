#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define MAX(a, b) (a >= b ? a : b)

typedef struct Component {
    unsigned int a, b, strength, maxlength, maxstrength;
    char used;
} Component;

void getmaxstrengths(unsigned int maxport, unsigned int componentssize,
            Component ***connections, Component *component,
            unsigned int fromport, unsigned int strength) {
    if (component == NULL) {
        unsigned int p;
        for (p = 0; p <= maxport; p++) {
            if (connections[fromport][p]) {
                getmaxstrengths(maxport, componentssize, connections,
                    connections[fromport][p], p, strength);
            }
        }
    } else if (!component->used) {
        component->used = 1;
        unsigned int newstrength = strength + component->strength;
        if (newstrength > component->maxstrength) {
            component->maxstrength = newstrength;
        }
        unsigned int p;
        for (p = 0; p <= maxport; p++) {
            if (connections[fromport][p]) {
                getmaxstrengths(maxport, componentssize, connections,
                    connections[fromport][p], p, newstrength);
            }
        }
        component->used = 0;
    }
}

void getmaxlengths(unsigned int maxport, unsigned int componentssize,
            Component ***connections, Component *component,
            unsigned int fromport, unsigned int length, unsigned int strength) {
    if (component == NULL) {
        unsigned int p;
        for (p = 0; p <= maxport; p++) {
            if (connections[fromport][p]) {
                getmaxlengths(maxport, componentssize, connections,
                    connections[fromport][p], p, length, strength);
            }
        }
    } else if (!component->used) {
        component->used = 1;
        unsigned int newlength = length + 1;
        unsigned int newstrength = strength + component->strength;
        if (newlength > component->maxlength ||
                    (newlength == component->maxlength &&
                        newstrength > component->maxstrength)) {
            component->maxlength = newlength;
            component->maxstrength = newstrength;
        }
        unsigned int p;
        for (p = 0; p <= maxport; p++) {
            if (connections[fromport][p]) {
                getmaxlengths(maxport, componentssize, connections,
                    connections[fromport][p], p, newlength, newstrength);
            }
        }
        component->used = 0;
    }
}

int main(int argc, char **argv) {
    unsigned int componentssize = 0;
    Component *components = NULL;
    char maxport = 0;

    // Get components
    unsigned long int length = 0;
    char *line = NULL;
    while ((length = getline(&line, &length, stdin)) != -1) {
        // printf("line = %s", line);
        components = realloc(components, (++componentssize) * sizeof(Component));

        char *endptr = NULL;
        components[componentssize - 1].a = strtol(line, &endptr, 10);
        components[componentssize - 1].b = strtol(&endptr[1], NULL, 10);
        // printf("a/b = %d/%d\n", components[componentssize - 1].a, components[componentssize - 1].b);
        components[componentssize - 1].used = 0;
        components[componentssize - 1].strength = components[componentssize - 1].a + components[componentssize - 1].b;
        components[componentssize - 1].maxlength = 0;
        components[componentssize - 1].maxstrength = 0;

        maxport = MAX(maxport, components[componentssize - 1].a);
        maxport = MAX(maxport, components[componentssize - 1].b);
    }
    free(line);

    // Make connections matrix
    Component ***connections = malloc((maxport + 1) * sizeof(Component **));
    unsigned int p;
    for (p = 0; p <= maxport; p++) {
        connections[p] = malloc((maxport + 1) * sizeof(Component *));
        memset(connections[p], 0, (maxport + 1) * sizeof(Component *));
    }

    // Fill connections matrix
    unsigned int c;
    for (c = 0; c < componentssize; c++) {
        connections[components[c].a][components[c].b] = &components[c];
        connections[components[c].b][components[c].a] = &components[c];
    }

    // Get maxstrengths
    getmaxstrengths(maxport, componentssize, connections, NULL, 0, 0);
    unsigned int maxstrength = 0;
    for (c = 0; c < componentssize; c++) {
        if (components[c].maxstrength > maxstrength) {
            maxstrength = components[c].maxstrength;
            printf("%d.maxstrength = %d\n", c, maxstrength);
        }
    }

    // Get maxlengths
    getmaxlengths(maxport, componentssize, connections, NULL, 0, 0, 0);
    unsigned int maxlength = 0;
    maxstrength = 0;
    for (c = 0; c < componentssize; c++) {
        if (components[c].maxlength > maxlength ||
                    (components[c].maxlength == maxlength &&
                        components[c].maxstrength > maxstrength)) {
            maxlength = components[c].maxlength;
            maxstrength = components[c].maxstrength;
            printf("%d.maxlength = %d, %d.maxstrength = %d\n", c, maxlength, c,
                maxstrength);
        }
    }

    return 0;
}
