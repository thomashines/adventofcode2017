#include "stdio.h"
#include "stdlib.h"
#include "string.h"

int compareChars (const void *a, const void *b) {
   return (*((char *) a)) - (*((char *) b));
}

int main (int argc, char **argv) {
  long unsigned int valids = 0;
  long unsigned int length;
  char *line = NULL;

  while ((length = getline(&line, &length, stdin)) != -1) {
    // printf("length = %u\n", length);
    // printf("line = %s", line);

    // Split line into words
    char **words = malloc(length * sizeof(char *));
    long unsigned int lineIndex = 0, wordIndex = 0, wordCount = 0;
    words[wordIndex] = line;
    while (lineIndex < length) {
      if (line[lineIndex] == ' ' || line[lineIndex] == '\n') {
        // End of a word
        line[lineIndex] = '\0';
        wordIndex++;
        words[wordIndex] = line + lineIndex + 1;
      }
      lineIndex++;
    }
    wordCount = wordIndex;

    // Sort the words
    for (wordIndex = 0; wordIndex < wordCount; wordIndex++) {
      qsort(words[wordIndex], strlen(words[wordIndex]), sizeof(char), compareChars);
      // printf("sorted = \"%s\"\n", words[wordIndex]);
    }

    // Compare the words
    char valid = 1;
    for (wordIndex = 0; wordIndex < wordCount - 1; wordIndex++) {
      long unsigned int compareWordIndex = 0;
      // printf("left = \"%s\"\n", words[wordIndex]);
      for (compareWordIndex = wordIndex + 1; compareWordIndex < wordCount; compareWordIndex++) {
        // printf("right = \"%s\"\n", words[compareWordIndex]);
        // printf("strcmp = %d\n", );
        if (strcmp(words[wordIndex], words[compareWordIndex]) == 0) {
          valid = 0;
          break;
        }
      }
      if (!valid) {
        break;
      }
    }

    // Sum
    if (valid) {
      valids++;
    }

    // Free words
    free(words);
  }
  free(line);

  // Report
  printf("valids = %lu\n", valids);

  return 0;
}
