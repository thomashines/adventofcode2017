#include "stdio.h"
#include "stdlib.h"
#include "string.h"

typedef enum Command {
    RCV,
    SND,
    ADD,
    JGZ,
    MOD,
    MUL,
    SET
} Command;

char *commands[] = {
    "rcv",
    "snd",
    "add",
    "jgz",
    "mod",
    "mul",
    "set"
};

typedef struct Instruction {
    Command command;
    char carg0, carg1;
    long long int iarg0, iarg1;
} Instruction;

long long int eval(long long int registers[], char carg0, long long int iarg0) {
    if (carg0) {
        return registers[carg0 - 'a'];
    }
    return iarg0;
}

int main(int argc, char **argv) {
    unsigned int instruction = 0;
    Instruction *instructions = NULL;

    long long int registers[26];
    memset(registers, 0, sizeof(long long int) * 26);

    long unsigned int length = 0;
    char *line = NULL;
    while((length = getline(&line, &length, stdin)) != -1) {
        // printf("line = %s", line);
        instructions = realloc(instructions, sizeof(Instruction) * (instruction + 1));
        memset(&instructions[instruction], 0, sizeof(Instruction));
        for (Command c = RCV; c <= SET; c++) {
            if (strncmp(line, commands[c], 3) == 0) {
                instructions[instruction].command = c;
                break;
            }
        }
        char *argptr = line + 4, *endptr;
        instructions[instruction].iarg0 = strtoll(argptr, &endptr, 10);
        instructions[instruction].carg0 = argptr == endptr ? argptr[0] : 0;
        if (instructions[instruction].command >= ADD) {
            argptr = argptr == endptr ? argptr + 2 : endptr + 1;
            instructions[instruction].iarg1 = strtoll(argptr, &endptr, 10);
            instructions[instruction].carg1 = argptr == endptr ? argptr[0] : 0;
        }
        // printf("%d: %d/%c, %d/%c\n", instructions[instruction].command,
        //     instructions[instruction].iarg0,
        //     instructions[instruction].carg0,
        //     instructions[instruction].iarg1,
        //     instructions[instruction].carg1);
        instruction++;
    }
    free(line);
    // return 0;

    int i = 0;
    char hasplayed = 0;
    int lastplayed;
    while (i >= 0 && i < instruction) {
        // printf("i = %d, command = %d\n", i, instructions[i].command);
        long long int val0 = eval(registers, instructions[i].carg0, instructions[i].iarg0);
        long long int val1 = eval(registers, instructions[i].carg1, instructions[i].iarg1);
        switch (instructions[i].command) {
            // Commands
            // rcv X: recovers the frequency of the last sound played, but only when the value of X is not zero. (If it is zero, the command does nothing.)
            case RCV:
                printf("rcv %d\n", val0);
                if (val0 != 0) {
                    if (hasplayed) {
                        printf("lastplayed = %d\n", lastplayed);
                    } else {
                        printf("hasplayed = %d\n", hasplayed);
                    }
                    i += instruction;
                }
                i++;
                break;
            // snd X: plays a sound with a frequency equal to the value of X.
            case SND:
                printf("snd %d\n", val0);
                hasplayed = 1;
                lastplayed = val0;
                i++;
                break;
            // add X Y: increases register X by the value of Y.
            case ADD:
                printf("add %c %d\n", instructions[i].carg0, val1);
                registers[instructions[i].carg0 - 'a'] += val1;
                i++;
                break;
            // jgz X Y: jumps with an offset of the value of Y, but only if the value of X is greater than zero. (An offset of 2 skips the next instruction, an offset of -1 jumps to the previous instruction, and so on.)
            case JGZ:
                printf("jgz %d %d\n", val0, val1);
                i += val0 > 0 ? val1 : 1;
                break;
            // mod X Y: sets register X to the remainder of dividing the value contained in register X by the value of Y (that is, it sets X to the result of X modulo Y).
            case MOD:
                printf("mod %c %d\n", instructions[i].carg0, val1);
                registers[instructions[i].carg0 - 'a'] = val0 % val1;
                i++;
                break;
            // mul X Y: sets register X to the result of multiplying the value contained in register X by the value of Y.
            case MUL:
                printf("mul %c %d\n", instructions[i].carg0, val1);
                registers[instructions[i].carg0 - 'a'] *= val1;
                i++;
                break;
            // set X Y: sets register X to the value of Y.
            case SET:
                printf("set %c %d\n", instructions[i].carg0, val1);
                registers[instructions[i].carg0 - 'a'] = val1;
                i++;
                break;
            default:
                printf("nah\n");
                i += instruction;
                break;
        }
    }

    free(instructions);
    return 0;
}
