#include "semaphore.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "pthread.h"

typedef struct Node Node;
struct Node {
    long long int value;
    Node *left;
    Node *right;
};

typedef struct Queue Queue;
struct Queue {
    sem_t unlocked;
    sem_t lengthsem;
    unsigned int length;
    unsigned int pushes;
    Node *head;
    Node *tail;
};

void pushback(Queue *queue, long long int value) {
    // printf("BA\n");

    // Lock queue
    sem_wait(&queue->unlocked);

    // Increment pushes
    queue->pushes++;

    // Make new node
    Node *new = malloc(sizeof(Node));
    new->value = value;
    new->right = NULL;

    // Make new node tail
    if (queue->tail != NULL) {
        queue->tail->right = new;
    }
    new->left = queue->tail;
    queue->tail = new;

    // Make new node head?
    if (queue->head == NULL) {
        queue->head = new;
    }

    // Increment length
    sem_post(&queue->lengthsem);
    queue->length++;
    // printf("queue->length = %d\n", queue->length);

    // printf("queue->length = %d\n", queue->length);
    // printf("    new->left = %p\n", new->left);
    // printf("          new = %p\n", new);
    // printf("   new->right = %p\n", new->right);

    // Unlock queue
    sem_post(&queue->unlocked);
}

long long int popfront(Queue *queue) {
    // printf("FA\n");
    // Wait for an element
    char popped = 0;
    while (!popped) {
        // Wait until queue has elements
        sem_wait(&queue->lengthsem);
        sem_post(&queue->lengthsem);

        // Lock queue
        sem_wait(&queue->unlocked);

        // If not empty, continue
        if (queue->length > 0) {
            break;
        }

        // Otherwise, unlock and restart
        sem_post(&queue->unlocked);
    }

    // Pop value
    // printf("FB\n");
    Node *head = queue->head;
    queue->head = queue->head->right;
    if (queue->head != NULL) {
        queue->head->left = NULL;
    }
    if (queue->tail == head) {
        queue->tail = NULL;
    }

    // Decrement length
    sem_post(&queue->lengthsem);
    queue->length--;

    // Unlock queue
    sem_post(&queue->unlocked);

    // Return value
    long long int value = head->value;
    free(head);
    return value;
}

Queue *makequeue(void) {
    Queue *queue = malloc(sizeof(Queue));
    sem_init(&queue->unlocked, 0, 1);
    sem_init(&queue->lengthsem, 0, 0);
    queue->head = NULL;
    queue->tail = NULL;
    return queue;
}

void printqueue(Queue *queue) {
    sem_wait(&queue->unlocked);
    Node *node = queue->head;
    while (node != NULL) {
        printf("%lld", node->value);
        node = node->right;
        if (node != NULL) {
            printf(", ");
        }
    }
    printf("\n");
    sem_post(&queue->unlocked);
}

void freequeue(Queue *queue) {
    Node *node = queue->head, *tofree;
    while (node != NULL) {
        tofree = node;
        node = node->right;
        // printf("       node %p\n", tofree);
        // printf("node->right %p\n", node);
        // printf("val %lld\n", tofree->value);
        free(tofree);
    }
    sem_destroy(&queue->unlocked);
    sem_destroy(&queue->lengthsem);
    free(queue);
}

typedef enum Command {
    RCV,
    SND,
    ADD,
    JGZ,
    MOD,
    MUL,
    SET
} Command;

char *commands[] = {
    "rcv",
    "snd",
    "add",
    "jgz",
    "mod",
    "mul",
    "set"
};

typedef struct Instruction {
    Command command;
    char carg0, carg1;
    long long int iarg0, iarg1;
} Instruction;

typedef struct Thread Thread;
struct Thread {
    long long int id;
    Queue *in;
    Queue *out;
    int length;
    Instruction *instructions;
};

long long int eval(long long int registers[], char carg0, long long int iarg0) {
    if (carg0) {
        return registers[carg0 - 'a'];
    }
    return iarg0;
}

void *thread(void *threadptr) {

    Thread *thread = (Thread *) threadptr;

    long long int registers[26];
    memset(registers, 0, sizeof(long long int) * 26);
    registers['p' - 'a'] = thread->id;

    int i = 0;
    while (i >= 0 && i < thread->length) {
        // printf("i = %d, command = %d\n", i, instructions[i].command);
        long long int val0 = eval(registers, thread->instructions[i].carg0,
            thread->instructions[i].iarg0);
        long long int val1 = eval(registers, thread->instructions[i].carg1,
            thread->instructions[i].iarg1);
        switch (thread->instructions[i].command) {
            // Commands
            // rcv X: recovers the frequency of the last sound played, but only when the value of X is not zero. (If it is zero, the command does nothing.)
            case RCV:
                printf("%lld->in->pushes = %lld\n", thread->id, thread->in->pushes);
                printf("%lld->out->pushes = %lld\n", thread->id, thread->out->pushes);
                // printf("%lld: rcv %c\n", thread->id,
                //     thread->instructions[i].carg0);
                registers[thread->instructions[i].carg0 - 'a'] = popfront(
                    thread->in);
                // printf("%d: rcv'd %d\n", thread->id,
                //     registers[thread->instructions[i].carg0 - 'a']);
                i++;
                break;
            // snd X: plays a sound with a frequency equal to the value of X.
            case SND:
                // printf("%lld: snd %lld\n", thread->id, val0);
                pushback(thread->out, val0);
                i++;
                break;
            // add X Y: increases register X by the value of Y.
            case ADD:
                // printf("add %c %lld\n", thread->instructions[i].carg0, val1);
                registers[thread->instructions[i].carg0 - 'a'] += val1;
                i++;
                break;
            // jgz X Y: jumps with an offset of the value of Y, but only if the value of X is greater than zero. (An offset of 2 skips the next instruction, an offset of -1 jumps to the previous instruction, and so on.)
            case JGZ:
                // printf("jgz %lld %lld\n", val0, val1);
                i += val0 > 0 ? val1 : 1;
                break;
            // mod X Y: sets register X to the remainder of dividing the value contained in register X by the value of Y (that is, it sets X to the result of X modulo Y).
            case MOD:
                // printf("mod %c %lld\n", thread->instructions[i].carg0, val1);
                registers[thread->instructions[i].carg0 - 'a'] = val0 % val1;
                i++;
                break;
            // mul X Y: sets register X to the result of multiplying the value contained in register X by the value of Y.
            case MUL:
                // printf("mul %c %lld\n", thread->instructions[i].carg0, val1);
                registers[thread->instructions[i].carg0 - 'a'] *= val1;
                // printf("mul'd\n");
                i++;
                break;
            // set X Y: sets register X to the value of Y.
            case SET:
                // printf("set %c %lld\n", thread->instructions[i].carg0, val1);
                registers[thread->instructions[i].carg0 - 'a'] = val1;
                i++;
                break;
            default:
                // printf("nah\n");
                i += thread->length;
                break;
        }
    }
    printf("%lld: done\n", thread->id);
    return NULL;
}

void *ezthread(void *threadptr) {
    Thread *thread = (Thread *) threadptr;
    for (int i = 0; i < 10; i++) {
        // printf("%lld.in = ", thread->id);
        // printqueue(thread->in);
        long long int value = popfront(thread->in);
        printf("%lld got %lld, sends %lld\n", thread->id, value, value + 1);
        pushback(thread->out, value + 1);
    }
    printf("%lld: done\n", thread->id);
    return NULL;
}

int main(int argc, char **argv) {
    unsigned int instruction = 0;
    Instruction *instructions = NULL;

    long unsigned int length = 0;
    char *line = NULL;
    while((length = getline(&line, &length, stdin)) != -1) {
        // printf("line = %s", line);
        instructions = realloc(instructions, sizeof(Instruction) * (instruction + 1));
        memset(&instructions[instruction], 0, sizeof(Instruction));
        for (Command c = RCV; c <= SET; c++) {
            if (strncmp(line, commands[c], 3) == 0) {
                instructions[instruction].command = c;
                break;
            }
        }
        char *argptr = line + 4, *endptr;
        instructions[instruction].iarg0 = strtoll(argptr, &endptr, 10);
        instructions[instruction].carg0 = argptr == endptr ? argptr[0] : 0;
        if (instructions[instruction].command >= ADD) {
            argptr = argptr == endptr ? argptr + 2 : endptr + 1;
            instructions[instruction].iarg1 = strtoll(argptr, &endptr, 10);
            instructions[instruction].carg1 = argptr == endptr ? argptr[0] : 0;
        }
        // printf("%d: %d/%c, %d/%c\n", instructions[instruction].command,
        //     instructions[instruction].iarg0,
        //     instructions[instruction].carg0,
        //     instructions[instruction].iarg1,
        //     instructions[instruction].carg1);
        instruction++;
    }
    free(line);

    // Set up threads
    Thread zero, one;
    zero.id = 0;
    one.id = 1;
    zero.instructions = instructions;
    one.instructions = instructions;
    zero.length = instruction;
    one.length = instruction;
    zero.in = makequeue();
    one.in = makequeue();
    zero.out = one.in;
    one.out = zero.in;

    // pushback(zero.in, 0);

    pthread_t threadzero, threadone;
    pthread_create(&threadzero, NULL, thread, &zero);
    // pthread_create(&threadzero, NULL, ezthread, &zero);
    pthread_create(&threadone, NULL, thread, &one);
    // pthread_create(&threadone, NULL, ezthread, &one);
    pthread_join(threadzero, NULL);
    pthread_join(threadone, NULL);

    freequeue(zero.in);
    freequeue(one.in);
    free(instructions);
    return 0;
}
