#!/usr/bin/env perl

use strict;
use warnings;

my $count = 0;
my $A = $ARGV[0];
my $B = $ARGV[1];
for (my $i = 0; $i <= 40000000; $i++) {
    $A = $A * 16807 % 2147483647;
    $B = $B * 48271 % 2147483647;
    if (substr(sprintf("%016b\n", $A), -17) == substr(sprintf("%016b\n", $B), -17)) {
        $count += 1;
    }
}
print "count = $count\n";
