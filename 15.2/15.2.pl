#!/usr/bin/env perl

use strict;
use warnings;

my $N = 5000000;

my $A = $ARGV[0];
my @AS; $AS[$N - 1] = 0;
my $AI = 0;
while ($AI < $N) {
    $A = $A * 16807 % 2147483647;
    if ($A % 4 == 0) {
        $AS[$AI++] = $A;
    }
}
print "generated A\n";

my $B = $ARGV[1];
my @BS; $BS[$N - 1] = 0;
my $BI = 0;
while ($BI < $N) {
    $B = $B * 48271 % 2147483647;
    if ($B % 8 == 0) {
        $BS[$BI++] = $B;
    }
}
print "generated B\n";

my $count = 0;
for (my $i = 0; $i < $N; $i++) {
    if (substr(sprintf("%016b\n", $AS[$i]), -17) == substr(sprintf("%016b\n", $BS[$i]), -17)) {
        $count += 1;
    }
}
print "count = $count\n";
