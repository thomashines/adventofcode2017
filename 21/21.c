#include "stdio.h"
#include "stdlib.h"
#include "string.h"

char *initial[] = {
    ".#.",
    "..#",
    "###"
};

char **squaremalloc(unsigned int size) {
    char **square = malloc(size * sizeof(char *));
    unsigned int y;
    for (y = 0; y < size; y++) {
        square[y] = malloc(size);
    }
    return square;
}

void squarefree(char **square, unsigned int size) {
    unsigned int y;
    for (y = 0; y < size; y++) {
        free(square[y]);
    }
    free(square);
}

void squareprint(char **square, unsigned int size) {
    unsigned int x, y;
    for (y = 0; y < size; y++) {
        for (x = 0; x < size; x++) {
            putc(square[y][x], stdout);
        }
        putc('\n', stdout);
    }
}

char **squareflip(char **square, unsigned int size, char horizontal, char vertical) {
    char **flipped = squaremalloc(size);
    unsigned int xi, yi, xo, yo;
    for (yi = 0; yi < size; yi++) {
        yo = vertical ? size - yi - 1 : yi;
        for (xi = 0; xi < size; xi++) {
            xo = horizontal ? size - xi - 1 : xi;
            flipped[yo][xo] = square[yi][xi];
        }
    }
    return flipped;
}

// Rotate 90 degrees clockwise
//  input -> output
// (       0,        0) -> (size - 1,        0)
// (size - 1,        0) -> (size - 1, size - 1)
// (size - 1, size - 1) -> (       0, size - 1)
// (       0, size - 1) -> (       0,        0)
char **squarerotate(char **square, unsigned int size) {
    char **rotated = squaremalloc(size);
    unsigned int xi, yi, xo, yo;
    for (yi = 0; yi < size; yi++) {
        xo = size - yi - 1;
        for (xi = 0; xi < size; xi++) {
            yo = xi;
            rotated[yo][xo] = square[yi][xi];
        }
    }
    return rotated;
}

char **squareload(unsigned int size, char *string) {
    char **square = squaremalloc(size);
    unsigned int x, y;
    for (y = 0; y < size; y++) {
        for (x = 0; x < size; x++) {
            square[y][x] = string[(size + 1) * y + x];
        }
    }
    return square;
}

char ****squaresplitmalloc(unsigned int splitsize, unsigned int partsize) {
    char ****split = malloc(splitsize * sizeof(char ***));
    unsigned int xs, ys;
    for (ys = 0; ys < splitsize; ys++) {
        split[ys] = malloc(splitsize * sizeof(char **));
        for (xs = 0; xs < splitsize; xs++) {
            split[ys][xs] = squaremalloc(partsize);
        }
    }
    return split;
}

char ****squaresplit(char **square, unsigned int size, unsigned int partsize) {
    char ****split = squaresplitmalloc(size / partsize, partsize);
    unsigned int x, y;
    for (x = 0; x < size; x++) {
        for (y = 0; y < size; y++) {
            split[y / partsize][x / partsize][y % partsize][x % partsize] =
                square[y][x];
        }
    }
    return split;
}

void squaresplitfree(char ****split, unsigned int splitsize, unsigned int partsize) {
    unsigned int x, y;
    for (y = 0; y < splitsize; y++) {
        for (x = 0; x < splitsize; x++) {
            squarefree(split[y][x], partsize);
        }
        free(split[y]);
    }
    free(split);
}

char squarecompare(char **a, char **b, unsigned int size) {
    unsigned int y;
    for (y = 0; y < size; y++) {
        if (strncmp(a[y], b[y], size)) {
            return 0;
        }
    }
    return 1;
}

void squarecopy(char **from, char **to, unsigned int size) {
    unsigned int y;
    for (y = 0; y < size; y++) {
        memcpy(to[y], from[y], size);
    }
}

char **squarejoin(char ****split, unsigned int splitsize, unsigned int partsize) {
    unsigned int joinedsize = splitsize * partsize;
    char **joined = squaremalloc(joinedsize);
    unsigned int x, y;
    for (y = 0; y < joinedsize; y++) {
        for (x = 0; x < joinedsize; x++) {
            joined[y][x] =
                split[y / partsize][x / partsize][y % partsize][x % partsize];
        }
    }
    return joined;
}

unsigned int squarecount(char **square, unsigned int size) {
    unsigned int count = 0, x, y;
    for (y = 0; y < size; y++) {
        for (x = 0; x < size; x++) {
            if (square[y][x] == '#') {
                count++;
            }
        }
    }
    return count;
}

// int rules;
// char froms[rules][transforms][size][size]
// char tos[rules][size][size]
// char split[splitsize][splitsize][size][size]

int main(int argc, char *argv[]) {
    unsigned int rules = 0;
    char ****froms = NULL, ***tos = NULL;
    unsigned int *sizes = NULL;

    unsigned int i, f, r, t, x, y;

    // Get rules
    unsigned long int length = 0;
    char *line = NULL;
    while ((length = getline(&line, &length, stdin)) != -1) {
        // printf("line[%lu] = %s", length, line);

        // Allocate another rule
        rules++;
        froms = realloc(froms, rules * sizeof(char ***));
        froms[rules - 1] = malloc(16 * sizeof(char **));
        memset(froms[rules - 1], 0, 16 * sizeof(char **));
        tos = realloc(tos, rules * sizeof(char **));
        sizes = realloc(sizes, rules * sizeof(unsigned int));

        // Get size
        sizes[rules - 1] = length == 22 ? 2 : 3;

        // Get input
        froms[rules - 1][0] = squareload(sizes[rules - 1], line);
        // printf("from\n");
        // squareprint(froms[rules - 1][0], sizes[rules - 1]);

        // Get output
        tos[rules - 1] = squareload(sizes[rules - 1] + 1,
            line + sizes[rules - 1] * (sizes[rules - 1] + 1) + 3);
        // printf("to\n");
        // squareprint(tos[rules - 1], sizes[rules - 1] + 1);
        // printf("\n");

        // Flip it
        froms[rules - 1][4] = squareflip(froms[rules - 1][0],
            sizes[rules - 1], 1, 0);
        froms[rules - 1][8] = squareflip(froms[rules - 1][0],
            sizes[rules - 1], 0, 1);
        froms[rules - 1][12] = squareflip(froms[rules - 1][0],
            sizes[rules - 1], 1, 1);

        // Rotate it
        // printf("from\n");
        for (f = 0; f < 16; f += 4) {
            froms[rules - 1][f + 1] = squarerotate(froms[rules - 1][f + 0],
                sizes[rules - 1]);
            froms[rules - 1][f + 2] = squarerotate(froms[rules - 1][f + 1],
                sizes[rules - 1]);
            froms[rules - 1][f + 3] = squarerotate(froms[rules - 1][f + 2],
                sizes[rules - 1]);
            // squareprint(froms[rules - 1][f + 0], sizes[rules - 1]);
            // squareprint(froms[rules - 1][f + 1], sizes[rules - 1]);
            // squareprint(froms[rules - 1][f + 2], sizes[rules - 1]);
            // squareprint(froms[rules - 1][f + 3], sizes[rules - 1]);
            // printf("\n");
        }
    }
    free(line);

    // Do the thing
    unsigned int size = 3;
    char **square = squareflip(initial, size, 0, 0);
    printf("size = %u\n", size);
    squareprint(square, size);
    printf("on = %u\n", squarecount(square, size));
    printf("\n");

    for (i = 0; i < 18; i++) {
        // Split
        unsigned int partsize = size % 2 ? 3 : 2;
        unsigned int splitsize = size / partsize;
        char ****split = squaresplit(square, size, partsize);

        // Refine
        char ****refined = squaresplitmalloc(splitsize, partsize + 1);
        for (y = 0; y < splitsize; y++) {
            for (x = 0; x < splitsize; x++) {
                // printf("from\n");
                // squareprint(split[y][x], partsize);
                char matched = 0;
                for (r = 0; r < rules; r++) {
                    if (sizes[r] == partsize) {
                        for (t = 0; t < 16; t++) {
                            if (squarecompare(split[y][x], froms[r][t],
                                        partsize)) {
                                squarecopy(tos[r], refined[y][x], partsize + 1);
                                // printf("to\n");
                                // squareprint(refined[y][x], partsize + 1);
                                matched = 1;
                                break;
                            }
                        }
                        if (matched) {
                            break;
                        }
                    }
                }
                if (!matched) {
                    printf("no match\n");
                    return 1;
                }
            }
        }

        // Join
        char **joined = squarejoin(refined, splitsize, partsize + 1);
        squarefree(square, size);
        square = joined;
        size = splitsize * (partsize + 1);
        printf("size = %u\n", size);
        squareprint(square, size);
        printf("on = %u\n", squarecount(square, size));
        printf("\n");

        // Free splits
        squaresplitfree(split, splitsize, partsize);
        squaresplitfree(refined, splitsize, partsize + 1);
    }

    // Free
    squarefree(square, size);
    for (r = 0; r < rules; r++) {
        for (t = 0; t < 16; t++) {
            if (froms[r][t] != NULL) {
                squarefree(froms[r][t], sizes[r]);
            }
        }
        free(froms[r]);
        squarefree(tos[r], sizes[r]);
    }
    free(froms);
    free(tos);
    free(sizes);

    return 0;
}
