#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define LENGTH 16
#define DANCES 100

void dance (char *steps, long unsigned int length, char *initial, char *final) {
    long unsigned int size, lefti, righti;
    char *split, *leftc, *rightc, swapc;
    char programs[LENGTH + 1], swap[LENGTH + 1];
    steps[length - 1] = '\0';
    memcpy(programs, initial, LENGTH + 1);
    memcpy(swap, initial, LENGTH + 1);
    for (char *c = steps; c < steps + length; c++) {
        split = strchr(c, ',');
        if (split != NULL) {
            *split = '\0';
        }
        switch (*(c++)) {
            case 's':
                size = strtol(c, &c, 10);
                memcpy(swap, programs + LENGTH - size, size);
                memcpy(swap + size, programs, LENGTH - size);
                memcpy(programs, swap, LENGTH);
                break;
            case 'x':
                lefti = strtol(c, &c, 10);
                righti = strtol(++c, &c, 10);
                swapc = programs[lefti];
                programs[lefti] = programs[righti];
                programs[righti] = swapc;
                break;
            case 'p':
                leftc = strchr(programs, *(c++));
                c++;
                rightc = strchr(programs, *(c++));
                swapc = *leftc;
                *leftc = *rightc;
                *rightc = swapc;
                break;
        }
    }
    memcpy(final, programs, LENGTH + 1);
}

int main (int argc, char **argv) {
    // Initial state
    char initial[LENGTH + 1], programs[LENGTH + 1];
    for (char *p = initial, c = 'a'; c < 'a' + LENGTH; *(p++) = c++);
    initial[LENGTH] = '\0';
    memcpy(programs, initial, LENGTH + 1);
    printf(" initial %s\n", initial);

    // Build list of states
    unsigned int statecount = 0;
    char **states = malloc(sizeof(char *));

    // Do dances
    long unsigned int length = 0;
    char *line = NULL, *linecopy = NULL;
    length = getline(&line, &length, stdin);
    linecopy = malloc(length + 1);
    for (unsigned int d = 0; d < DANCES; d++) {
        printf("%10d %s\n", d, programs);
        char looped = 0;
        for (int s = 0; s < statecount; s++) {
            // printf("strncmp? %s\n", from[0]);
            if (strncmp(programs, states[s], LENGTH) == 0) {
                // memcpy(programs, to[t], LENGTH + 1);
                // printf("memcpy\n");
                looped = 1;
                break;
            }
            // printf("strncmp\n");
        }
        if (!looped) {
            // Copy into states
            states = realloc(states, (++statecount) * sizeof(char *));
            states[statecount - 1] = malloc(LENGTH + 1);
            memcpy(states[statecount - 1], programs, LENGTH + 1);

            // Do dance
            memcpy(linecopy, line, length + 1);
            dance(linecopy, length, programs, programs);
        } else {
            printf("     final %s\n", states[DANCES % d]);
            break;
        }
    }
    free(line);
    free(linecopy);
}
