import 'dart:convert';
import 'dart:io';

void main() {
    stdin.transform(UTF8.decoder)
        .transform(new LineSplitter())
        .map((line) => line.split(": ").map(int.parse))
        .toList()
        .then((scanners) => print(solve(scanners)));
}

Iterable<int> get integers sync* {
    int i = 0;
    while (true) yield i++;
}

void solve(List<List<int>> scanners) =>
    integers.firstWhere((delay) =>
        !scanners.any((scanner) {
            int depth = scanner.first;
            int range = scanner.last;
            return (depth + delay - range + 1) % (2 * range - 2) == range - 1;
        })
    );
