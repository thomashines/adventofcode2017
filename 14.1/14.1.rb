seed = ARGV[0]
sum = 0
(0..127).each do |n|
    lineseed = "#{seed}-#{n}"
    hash = `echo "#{lineseed}" | scala TenTwo`.strip
    bits = hash.to_i(16).to_s(2).rjust(128, "0").gsub("0", ".").gsub("1", "#")
    ones = bits.count("#")
    sum += ones
    puts "#{lineseed}: #{bits}: #{ones}: #{sum}"
end
