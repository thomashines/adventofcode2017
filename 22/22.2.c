#include "stdio.h"
#include "stdlib.h"

typedef enum state {CLEAN, WEAK, INFECTED, FLAG} state;

typedef struct Node {
    int x, y;
    state infected;
} Node;

int comparexy(int x0, int y0, int x1, int y1) {
    if (x0 == x1) {
        return y0 - y1;
    }
    return x0 - x1;
}

int nodecompare(const void *a, const void *b) {
    Node *nodea = (Node *) a, *nodeb = (Node *) b;
    return comparexy(nodea->x, nodea->y, nodeb->x, nodeb->y);
}

typedef struct Nodes {
    unsigned int count, size;
    Node *nodes;
    char sorted;
} Nodes;

Nodes *nodesmake(void) {
    Nodes *nodes = malloc(sizeof(Nodes));
    nodes->count = 0;
    nodes->size = 2;
    nodes->nodes = malloc(nodes->size * sizeof(Node));
    nodes->sorted = 1;
    return nodes;
}

void nodesfree(Nodes *nodes) {
    free(nodes->nodes);
    free(nodes);
}

Node *nodesgetsub(Node *nodes, int x, int y, unsigned int i0, unsigned int i1) {
    // End cases
    if (i0 == i1) {
        if (comparexy(nodes[i0].x, nodes[i0].y, x, y) == 0) {
            return &nodes[i0];
        } else {
            return NULL;
        }
    } else if (i1 - i0 == 1) {
        if (comparexy(nodes[i0].x, nodes[i0].y, x, y) == 0) {
            return &nodes[i0];
        } else if (comparexy(nodes[i1].x, nodes[i1].y, x, y) == 0) {
            return &nodes[i1];
        } else {
            return NULL;
        }
    }

    // Get middle
    unsigned int mid = (i0 + i1) / 2;
    int comp = comparexy(nodes[mid].x, nodes[mid].y, x, y);

    // Return or recurse
    if (comp == 0) {
        return &nodes[mid];
    } else if (comp > 0) {
        // Target is to the left
        return nodesgetsub(nodes, x, y, i0, mid);
    } else if (comp < 0) {
        // Target is to the right
        return nodesgetsub(nodes, x, y, mid, i1);
    }

    // Else not found
    return NULL;
}

Node *nodesget(Nodes *nodes, int x, int y) {
    if (nodes->count == 0) {
        return NULL;
    }
    return nodesgetsub(nodes->nodes, x, y, 0, nodes->count - 1);
}

void nodesput(Nodes *nodes, int x, int y, state infected) {
    Node *node = nodesget(nodes, x, y);
    if (node != NULL) {
        node->infected = infected;
    } else {
        nodes->count++;
        if (nodes->count >= nodes->size) {
            nodes->size *= 2;
            nodes->nodes = realloc(nodes->nodes, nodes->size * sizeof(Node));
        }
        nodes->nodes[nodes->count - 1].x = x;
        nodes->nodes[nodes->count - 1].y = y;
        nodes->nodes[nodes->count - 1].infected = infected;
        nodes->sorted = 0;
    }
}

void nodessort(Nodes *nodes) {
    qsort(nodes->nodes, nodes->count, sizeof(Node), nodecompare);
    nodes->sorted = 1;
}

void nodesprint2d(Nodes *nodes, int x0, int y0, int x1, int y1) {
    Node *node;
    int x, y;
    for (y = y0; y <= y1; y++) {
        for (x = x0; x <= x1; x++) {
            node = nodesget(nodes, x, y);
            if (node == NULL) {
                putc('.', stdout);
            } else {
                switch (node->infected) {
                    case CLEAN: putc('.', stdout); break;
                    case WEAK: putc('W', stdout); break;
                    case INFECTED: putc('#', stdout); break;
                    case FLAG: putc('F', stdout); break;
                }
            }
        }
        putc('\n', stdout);
    }
}

void nodesprint1d(Nodes *nodes) {
    unsigned int i;
    for (i = 0; i < nodes->count; i++) {
        printf("(%d,%d,%d)",
            nodes->nodes[i].x,
            nodes->nodes[i].y,
            nodes->nodes[i].infected);
        if (i + 1 < nodes->count) {
            printf(",");
        }
    }
    printf("\n");
}

int main(int argc, char *argv[]) {
    Nodes *nodes = nodesmake();

    // Get rules
    int x, y = 0, dx, dy, dswap;
    unsigned long int length = 0, size = 0;
    char *line = NULL;
    while ((length = getline(&line, &length, stdin)) != -1) {
        // printf("line[%lu] = %s", length, line);
        if (length - 1 > size) {
            size = length - 1;
        }
        for (x = 0; x < length; x++) {
            if (line[x] == '#') {
                nodesput(nodes, x, y, INFECTED);
            }
        }
        y++;
    }
    free(line);


    // Print state
    nodessort(nodes);
    nodesprint2d(nodes, 0, 0, 2, 2);

    // Do infections
    x = (size - 1) / 2;
    y = (size - 1) / 2;
    printf("(%d,%d)\n", x, y);
    nodesprint2d(nodes, -8, -8, 8, 8);
    dx = 0;
    dy = -1;
    unsigned int burst, infections = 0;
    for (burst = 0; burst < 10000000; burst++) {
    // for (burst = 0; burst < 10000; burst++) {
    // for (burst = 0; burst < 100; burst++) {
        Node *node = nodesget(nodes, x, y);
        if (node == NULL || node->infected == CLEAN) {
            // Turn left
            dswap = dx;
            dx = dy;
            dy = -dswap;
            // Weaken
            nodesput(nodes, x, y, WEAK);
        } else if (node->infected == WEAK) {
            // Don't turn
            // Infect
            nodesput(nodes, x, y, INFECTED);
            infections++;
        } else if (node->infected == INFECTED) {
            // Turn right
            dswap = dx;
            dx = -dy;
            dy = dswap;
            // Flag
            nodesput(nodes, x, y, FLAG);
        } else if (node->infected == FLAG) {
            // Reverse
            dx = -dx;
            dy = -dy;
            // Clean
            nodesput(nodes, x, y, CLEAN);
        }
        if (node == NULL) {
            nodessort(nodes);
        }
        // Move
        x += dx;
        y += dy;
        // printf("(%d,%d)\n", x, y);
        // nodesprint2d(nodes, -8, -8, 8, 8);
        if (burst % 100000 == 0) {
            printf("burst = %u (%u%%)\n", burst, 100 * burst / 10000000);
            printf("infections = %u\n", infections);
        }
    }

    printf("infections = %u\n", infections);

    // Free
    nodesfree(nodes);
}
