package main

import "bufio"
import "fmt"
import "os"
import "strconv"
import "strings"

func main() {
    reader := bufio.NewReader(os.Stdin)
    text, _ := reader.ReadString('\n')
    bankStrings := strings.Split(text[:len(text) - 1], "\t")
    bankCount := len(bankStrings)
    banks := make([]byte, bankCount)
    for i := 0; i < bankCount; i++ {
        c, _ := strconv.Atoi(bankStrings[i])
        banks[i] = byte(c)
    }
    history := make(map[string]int)
    step := 0
    banksString := string(banks)
    for looped := false; !looped; _, looped = history[banksString] {
        history[banksString] = step
        max := banks[0]
        maxIndex := 0
        for i := 1; i < bankCount; i++ {
            if banks[i] > max {
                max = banks[i]
                maxIndex = i
            }
        }
        banks[maxIndex] = 0
        for ; max > 0; max-- {
            maxIndex = (maxIndex + 1) % bankCount
            banks[maxIndex] += 1
        }
        banksString = string(banks)
        step++
    }
    fmt.Printf("step = %d\n", step)
    fmt.Printf("history[banksString] = %d\n", history[banksString])
    fmt.Printf("loop size = %d\n", step - history[banksString])
}
