#!/usr/bin/env python

import sys
import math

def getDXY(p):
    side = math.ceil(math.sqrt(p))
    if side % 2 == 0:
        side += 1
    position = p - ((side - 2) ** 2) - 1
    if position < 0:
        position = 0
    corners = [
        1 * (side - 1) - 1,
        2 * (side - 1) - 1,
        3 * (side - 1) - 1,
        4 * (side - 1) - 1]
    if position < corners[0]:
        return (0, 1)
    elif position < corners[1]:
        return (-1, 0)
    elif position < corners[2]:
        return (0, -1)
    elif position <= corners[3]:
        return (1, 0)
    return (1, 0)

if __name__ == "__main__":
    print("3.2")
    threshold = int(sys.argv[1])
    print("threshold", threshold)
    vs = {(0,0):1}
    p = 0
    x = 0
    y = 0
    v = 1
    while v < threshold:
        p += 1
        (dx, dy) = getDXY(p)
        x += dx
        y += dy
        v = 0
        v += vs.get((x + 1, y), 0)
        v += vs.get((x + 1, y + 1), 0)
        v += vs.get((x, y + 1), 0)
        v += vs.get((x - 1, y + 1), 0)
        v += vs.get((x - 1, y), 0)
        v += vs.get((x - 1, y - 1), 0)
        v += vs.get((x, y - 1), 0)
        v += vs.get((x + 1, y - 1), 0)
        vs[(x, y)] = v
    print("v", v)
