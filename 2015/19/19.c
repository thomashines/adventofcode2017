#include "stdio.h"
#include "stdlib.h"
#include "string.h"

typedef struct StringSet {
    char **values;
    unsigned int count;
    unsigned int size;
} StringSet;

StringSet *ssmalloc(void) {
    StringSet *ss = malloc(sizeof(StringSet));
    ss->values = malloc(2 * sizeof(char *));
    ss->count = 0;
    ss->size = 2;
    return ss;
}

char *ssget(StringSet *ss, char *value) {
    unsigned int s;
    for (s = 0; s < ss->count; s++) {
        if (strcmp(value, ss->values[s]) == 0) {
            return ss->values[s];
        }
    }
    return NULL;
}

void ssput(StringSet *ss, char *value) {
    unsigned int s;
    for (s = 0; s < ss->count; s++) {
        if (strcmp(value, ss->values[s]) == 0) {
            ss->values[s] = value;
            return;
        }
    }
    ss->count++;
    if (ss->count > ss->size) {
        ss->size *= 2;
        ss->values = realloc(ss->values, ss->size * sizeof(char *));
    }
    ss->values[ss->count - 1] = value;
}

int main(int argc, char **argv) {
    long unsigned int length = 0;
    char *line = NULL;

    // Get rules
    unsigned int rulescount = 0, rulessize = 2;
    char **rulesin = malloc(rulessize * sizeof(char *));
    unsigned int *rulesinlen = malloc(rulessize * sizeof(unsigned int));
    char **rulesout = malloc(rulessize * sizeof(char *));
    unsigned int *rulesoutlen = malloc(rulessize * sizeof(unsigned int));
    while ((length = getline(&line, &length, stdin)) > 1) {
        // printf("line[%ld] = %s", length, line);
        // Allocate
        rulescount++;
        if (rulescount > rulessize) {
            rulessize *= 2;
            rulesin = realloc(rulesin, rulessize * sizeof(char *));
            rulesinlen = realloc(rulesinlen, rulessize * sizeof(unsigned int));
            rulesout = realloc(rulesout, rulessize * sizeof(char *));
            rulesoutlen = realloc(rulesoutlen, rulessize * sizeof(unsigned int));
        }

        // In rule
        char *in = line;
        char *end = strchr(in, ' ');
        *end = '\0';
        rulesinlen[rulescount - 1] = strlen(in);
        rulesin[rulescount - 1] = malloc((rulesinlen[rulescount - 1] + 1) * sizeof(char));
        memcpy(rulesin[rulescount - 1], in, rulesinlen[rulescount - 1] + 1);
        // printf("rulesin[%d][%d] = %s\n", rulescount - 1, rulesinlen[rulescount - 1], rulesin[rulescount - 1]);

        // Out rule
        char *out = &end[4];
        end = strchr(out, '\n');
        *end = '\0';
        rulesoutlen[rulescount - 1] = strlen(out);
        rulesout[rulescount - 1] = malloc((rulesoutlen[rulescount - 1] + 1) * sizeof(char));
        memcpy(rulesout[rulescount - 1], out, rulesoutlen[rulescount - 1] + 1);
        // printf("rulesout[%d][%d] = %s\n", rulescount - 1, rulesoutlen[rulescount - 1], rulesout[rulescount - 1]);
    }
    // printf("rulescount = %d\n", rulescount);

    // Get initial state
    length = getline(&line, &length, stdin) - 1;
    char *molecule = malloc((length + 1) * sizeof(char));
    memcpy(molecule, line, length);
    molecule[length] = '\0';
    // printf("molecule[%ld] = %s\n", length, molecule);

    // For each start of string to replace
    StringSet *molecules = ssmalloc();
    unsigned int f, r;
    for (f = 0; f < length; f++) {
        // For each rule
        for (r = 0; r < rulescount; r++) {
            // Check if it can apply
            // printf("f = %d, r = %d\n", f, r);
            // printf("&molecule[f] = %s, rulesin[r] = %s\n", &molecule[f], rulesin[r]);
            if (strncmp(&molecule[f], rulesin[r], rulesinlen[r]) == 0) {
                // printf("match f = %d, r = %d\n", f, r);
                // Get new size
                unsigned int size = length - rulesinlen[r] + rulesoutlen[r];
                // Make new molecule
                char *new = malloc((size + 1) * sizeof(char));
                memcpy(new, molecule, f);
                memcpy(&new[f], rulesout[r], rulesoutlen[r]);
                memcpy(&new[f + rulesoutlen[r]], &molecule[f + rulesinlen[r]], length - f - rulesinlen[r]);
                // printf("new = %s\n", new);
                ssput(molecules, new);
            }
        }
    }
    printf("molecules->count = %d\n", molecules->count);

    free(line);
    return 0;
}
